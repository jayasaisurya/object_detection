import cv2

def video_to_frames(video_path, output_path, frame_rate):
    # Open the video file
    video = cv2.VideoCapture(video_path)
    
    # Get the frames per second (fps) of the video
    fps = video.get(cv2.CAP_PROP_FPS)
    
    # Calculate the frame interval based on desired frame rate
    frame_interval = round(fps / frame_rate)
    
    # Initialize frame count and current frame index
    frame_count = 0
    current_frame_index = 0
    
    # Read the video frames
    while True:
        # Read the next frame
        ret, frame = video.read()
        
        # If the frame is not retrieved successfully, exit the loop
        if not ret:
            break
        
        # Increment the frame count
        frame_count += 1
        
        # Skip frames if needed to achieve the desired frame rate
        if frame_count % frame_interval != 0:
            continue
        
        # Save the frame as an image
        frame_path = f"{output_path}/frame_{current_frame_index}.jpg"
        cv2.imwrite(frame_path, frame)
        
        # Increment the current frame index
        current_frame_index += 1
    
    # Release the video file
    video.release()

# Specify the path to the video file
video_path = 'nestle_advertisement.mp4'

# Specify the output folder where the frames will be saved
output_folder = 'Test_Dataset'

# Specify the desired frame rate (e.g., 30 fps)
frame_rate = 30

# Convert the video to frames
video_to_frames(video_path, output_folder, frame_rate)
